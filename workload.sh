#!/bin/bash

workdir="/home/ubuntu/OpenFOAM/1/run/cavity"
dumpfile="/tmp/dumpFoam.log"
logfile="/dev/pts/2"
metricfile="/tmp/foamExecTime"
nNodes=8	# must be in sync. with system/decomposeParDict


generate_knob_file()
{
	cat <<- HERE
		domain:
		  common:
		    metrics:
		      shell.foamExecTime:
		        aggregated: false
		        sample_script: cat ${metricfile}

		    include_metrics: [shell.*]
		    target: shell.foamExecTime:min
		    plugins: [libshellplugin.so]
	HERE
}


cd ${workdir}

if [[ "$1" == "--cleanup" ]]; then
	git clean -xfd

	:> ${dumpfile}
	:> ${logfile}
	:> ${metricfile}

	exit 0
elif [[ "$1" == "--knob-file" ]]; then
	generate_knob_file
	exit 0
elif [[ "$1" == "--help" ]]; then
	echo "OpenFOAM: Optimizer-Studio workload script"
	echo "Command line options:"
	echo "NONE         Run OpenFOAM benchmark once."
	echo "--cleanup    Clean up all the auxiliary files."
	echo "--knob-file  Output the appropriate knob file. Example: $(basename $0) --knob-file > foam.yaml"
	echo "--help       Display this screen."

	exit 0
fi


# clean-up
git clean -xfd > /dev/null

# pre-set
printf " ----- Optimizer-Studio test run ---------------\n\n" >> ${dumpfile}
blockMesh >> ${dumpfile}
decomposePar -force >> ${dumpfile}

# actual calculation
mpiexec --allow-run-as-root -np ${nNodes} icoFoam -parallel >> ${dumpfile}

# gather execution time
execTime=$(tail -n 10 ${dumpfile} | awk '/^ExecutionTime/ {print $3}')
echo ${execTime} >> ${logfile}
echo ${execTime} > ${metricfile}

