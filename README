Hardware setup
--------------

AWS type:	r4.2xlarge (172.31.20.114)  
OS:		Ubuntu 16.04  


Installation procedure
----------------------

1.  Install OpenFOAM package for Ubuntu

add-apt-repository http://dl.openfoam.org/ubuntu  
sh -c "wget -O - http://dl.openfoam.org/gpg.key | apt-key add -"  
apt update  
apt install -y apt-transport-https  
apt install -y openfoam5  

2.  Setup environment variables

echo "source /opt/openfoam5/etc/bashrc" >> ~/.bashrc  
source ~/.bashrc  

3.  Test installation

simpleFoam -help  # should just run  

4.  Adapt for benchmarking purposes

mkdir -p ~/OpenFOAM/1  
cd ~/OpenFOAM/1  
git clone git@bitbucket.org:datarcs/benchmark-openfoam.git run  
OR  
git clone https://<bitbucket user name>@bitbucket.org/datarcs/benchmark-openfoam.git run  


The test
--------

cd run  

1.  Generate the knob file

./workload.sh --knob-file > foam.yaml  

2.  Run the test

optimizer-studio --sampling-mode=sync --knobs=embedded --knobs=./foam.yaml  ./workload.sh  


Test results
------------

1.  Optimizer-Studio output

Progress: 34%  
Best known knob settings so far:  
fs.file-max: 803154  
kernel.shmall: 15720001  
kernel.sched_min_granularity_ns: 10000000  
symmetric_multithreading: 0  
kernel.sched_wakeup_granularity_ns: 50000000  
Anticipated improvement: -42.64%  

2.  Benchmark execution time (Sec)

baseline:                  626.22  
                           622.87  
                           635.05  
                           ...  
progress 34%:              422.22  
                           435.2  
                           377.79  
Ctrl-C, back to baseline:  635.83  
                           637.62  
Apply best settings:       386.55  
                           413.64  
                           434.79  


Known issues
------------

1.  Optimizer-studio reports negative anticipated improvement. While the sign is inverted, the absolute value of the improvement is correct.


Reference
---------

[1] https://cfd.direct/openfoam/user-guide/cavity/#x5-40002.1  
[2] https://www.pugetsystems.com/labs/hpc/OpenFOAM-performance-on-Quad-socket-Xeon-and-Opteron-587/  

